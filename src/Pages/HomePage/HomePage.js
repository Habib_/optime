import React, { useState, useEffect } from 'react';
import {
    HubConnectionBuilder,
    LogLevel,
    HttpTransportType
} from '@microsoft/signalr';
import styles from './HomePage.module.css';
import { Helmet } from 'react-helmet'
import Auxiliary from '../../Components/HOC/Auxiliary'

const HomePage = () => {

    const ConState = {
        'FAIL': 'fail',
        'SUCCESS': 'success',
        'CONNECTING': 'connecting',
    }

    const [Detail, setDetail] = useState([]);
    const [ConnectionState, setConnectionState] = useState(ConState.CONNECTING)


    useEffect(() => {
        setConnectionState(ConState.CONNECTING)
        const connection = new HubConnectionBuilder()
            .configureLogging(LogLevel.Debug)
            .withUrl("http://94.182.191.41:5000/test_channel", {
                skipNegotiation: true,
                transport: HttpTransportType.WebSockets
            })
            .withAutomaticReconnect()
            .build();

        connection.start()
            .then(result => {
                console.log('Connected!');
                setConnectionState(ConState.SUCCESS)

                connection.on('OnTest', NewDetail => {
                    setDetail(Detail => [NewDetail, ...Detail]);
                });
            })
            .catch(e => {
                console.log('Connection failed: ', e)
                setConnectionState(ConState.FAIL)
            });
    }, []);

    useEffect(() => {
        if (Detail.length > 10) {
            setDetail(Detail.splice(10,))
        }
    }, [Detail])

    console.log(Detail)

    return (
        <Auxiliary>
            <Helmet>
                <title>OP - time</title>
            </Helmet>


            {
                ConnectionState === ConState.CONNECTING ?
                    <Auxiliary>
                        <h3> Connecting ... </h3>
                        <p className={styles.text}>please Wait</p>
                    </Auxiliary>
                    :
                    ConnectionState === ConState.FAIL ?
                        <Auxiliary>
                            <h3> Oops!!! </h3>
                            <p className={styles.text}>Something went wrong</p>
                        </Auxiliary>
                        :
                        <table>
                            <thead>
                                <tr>
                                    <th>Coordinates</th>
                                    <th>Speed</th>
                                    <th>DriverName</th>
                                </tr>
                            </thead>
                            {
                                Detail.length > 0 ?
                                    <tbody>
                                        <tr>
                                            <td>
                                                {Detail[0].coordinates[0]}
                                                <br />
                                                {Detail[0].coordinates[1]}
                                            </td>
                                            <td>{Detail[0].speed}</td>
                                            <td>{Detail[0].driverName}</td>
                                        </tr>
                                    </tbody>
                                    :
                                    null
                            }
                        </table>
            }


        </Auxiliary>
    );
};

export default HomePage;
