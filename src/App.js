import { Route, Switch } from "react-router";
import HomePage from "./Pages/HomePage/HomePage";

function App() {
  return (
    <Switch>
      <Route path="/" exact component={HomePage} />
    </Switch>
  );
}

export default App;
